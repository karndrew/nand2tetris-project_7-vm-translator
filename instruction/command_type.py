from enum import Enum

class CommandType(Enum):

    C_ARITHMETIC = "arithmetic"
    C_PUSH = "push"
    C_POP = "pop"
    C_LABEL = "label"
    C_GOTO = "goto"
    C_IF = "if-goto"
    C_FUNCTION = "function"
    C_RETURN = "return"
    C_CALL = "call"

    @staticmethod
    def is_arithmetic(command):
        arithmetic_logical_commands = ['add', 'sub', 'neg', 'eq', 'gt', 'lt', 'and', 'or', 'not']
        return command in arithmetic_logical_commands
