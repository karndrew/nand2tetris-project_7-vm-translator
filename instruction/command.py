from instruction.command_type import CommandType

class Command:

    command_mappings = {
        'lt': 'JLT',
        'gt': 'JGT',
        'eq': 'JEQ',

        'not': '!',
        'neg': '-',

        'or': '|',
        'and': '&',
        'add': '+',
        'sub': '-'
    }

    segment_mappings = {
        'constant': '@SP',
        'local': '@LCL',
        'argument': '@ARG',
        'this': '@THIS',
        'that': '@THAT',
        'pointer 0': '@THIS',
        'pointer 1': '@THAT',
    }

    call_command_number_map = {}

    arithmetic_commands = ['add', 'sub', 'or', 'and']
    logical_commands = ['lt', 'gt', 'eq']
    negation_commands = ['not', 'neg']
    branching_commands = ['label', 'goto', 'if-goto']
    function_commands = ['function', 'call', 'return']
    stack_commands = ['push', 'pop']

    command_number = 0
    current_function_name = 'Sys.init'
    filename = ''

    @classmethod
    def generate(cls, command, variable='', value=0):
        translated_command = ''
        if cls.__is_arithmetic(command):
            translated_command = cls.__generate_arithmetic(command)
        elif cls.__is_stack_command(command):
            translated_command = cls.__generate_stack_command(command, variable, value)
        elif cls.__is_logical(command):
            translated_command = cls.__generate_logical(command)
        elif cls.__is_negation(command):
            translated_command = cls.__generate_negation(command)
        elif cls.__is_branching(command):
            translated_command = cls.__generate_branching(command, variable)
        elif cls.__is_function(command):
            translated_command = cls.__generate_function(command, variable, value)

        cls.command_number += 1
        return translated_command

    @classmethod
    def __generate_arithmetic(cls, command):
        return ("@SP\n"
                "AM=M-1\n"
                "D=M\n"
                "A=A-1\n"
                "M=M{}D\n").format(cls.command_mappings[command])

    @classmethod
    def __generate_negation(cls, command):
        return ("@SP\n"
                "A=M-1\n"
                "M={}M\n").format(cls.command_mappings[command])

    @classmethod
    def __generate_logical(cls, command):
        suffix = '_' + command + str(cls.command_number)
        cls.command_number += 1
        return ("@SP\n"
                "AM=M-1\n"
                "D=M\n"
                "A=A-1\n"
                "D=M-D\n"
                "@TRUE{suffix}\n"
                "D;{jump_command}\n"
                "@SP\n"
                "A=M-1\n"
                "M=0\n"
                "@CONTINUE{suffix}\n"
                "0;JMP\n"
                "(TRUE{suffix})\n"
                "@SP\n"
                "A=M-1\n"
                "M=-1\n"
                "(CONTINUE{suffix})\n").format(jump_command=cls.command_mappings[command],
                                                             suffix=suffix)

    @classmethod
    def __generate_branching(cls, command, label):
        command_type = CommandType(command)

        if command_type is CommandType.C_LABEL:
            return ("({}${})\n").format(cls.current_function_name, label)
        elif command_type is CommandType.C_GOTO:
            return ("@{}${}\n"
                    "0;JMP\n").format(cls.current_function_name, label)
        elif command_type is CommandType.C_IF:
            return ("@SP\n"
                    "AM=M-1\n"
                    "D=M\n"
                    "@{}${}\n"
                    "D;JNE\n").format(cls.current_function_name, label)

    @classmethod
    def __generate_function(cls, command, function_name, num_vars):
        command_type = CommandType(command)

        if command_type is CommandType.C_FUNCTION:
            return cls.__generate_function_command(function_name, num_vars)
        elif command_type is CommandType.C_CALL:
            return cls.__generate_call_command(function_name, num_vars)
        elif command_type is CommandType.C_RETURN:
            return cls.__generate_return_command()

    @classmethod
    def __generate_function_command(cls, function_name, num_vars):
        function_command = ("({})\n").format(function_name)

        local_vars_number = int(num_vars)
        current_local_var_number = 0

        while current_local_var_number < local_vars_number:
            function_command += cls.__push_command('constant', 0)
            current_local_var_number += 1

        cls.current_function_name = function_name

        return function_command

    @classmethod
    def __generate_call_command(cls, function_name, num_args):

        cls.__update_current_call_index(function_name)

        return_label = ('{}$ret.{}').format(function_name, str(cls.call_command_number))
        call_command = ''

        # push return address
        call_command += ("//push return address\n"
                         "@{}\n"
                         "D=A\n").format(return_label)

        call_command += cls.__push_to_stack()

        # save LCL of the caller
        call_command += ("//push LCL\n"
                         "@LCL\n"
                         "D=M\n")

        call_command += cls.__push_to_stack()

        # save ARG of the caller
        call_command += ("//push ARG\n"
                         "@ARG\n"
                         "D=M\n")

        call_command += cls.__push_to_stack()

        # save THIS of the caller
        call_command += ("//push THIS\n"
                         "@THIS\n"
                         "D=M\n")

        call_command += cls.__push_to_stack()

        # save THAT of the caller
        call_command += ("//push THAT\n"
                         "@THAT\n"
                         "D=M\n")

        call_command += cls.__push_to_stack()

        # reposition ARG
        # ARG = SP - 5 - num_args
        call_command += ("//ARG = SP - 5 - nArgs\n"
                         "@SP\n"
                         "D=M\n"
                         "@5\n"
                         "D=D-A\n"
                         "@{}\n"
                         "D=D-A\n"
                         "@ARG\n"
                         "M=D\n\n").format(str(num_args))

        # reposition LCL
        call_command += ("//LCL = SP\n"
                         "@SP\n"
                         "D=M\n"
                         "@LCL\n"
                         "M=D\n\n")

        # goto function name. transfers control to the called function
        call_command += ("//goto functionName\n"
                         "@{}\n"
                         "0;JMP\n\n").format(function_name)

        # declares a label for the return-address
        call_command += ("//(returnAddress)\n"
                         "({})\n").format(return_label)

        return call_command

    @classmethod
    def __generate_return_command(cls):
        return_command = ''

        # endFrame is a temporary variable
        return_command += ("//endFrame = LCL\n"
                         "@LCL\n"
                         "D=M\n"
                         "@FRAME\n"
                         "M=D\n\n")

        # gets the return address
        return_command += ("//retAddr = *(endFrame - 5)\n"
                           "@FRAME\n"
                           "D=M\n"
                           "@5\n"
                           "D=D-A\n"
                           "A=D\n"
                           "D=M\n"
                           "@R14\n"
                           "M=D\n\n")

        # repositions the return value for the caller
        return_command += ("//*ARG = pop()\n"
                           "@SP\n"
                           "M=M-1\n"
                           "A=M\n"
                           "D=M\n"
                           "@ARG\n"
                           "A=M\n"
                           "M=D\n\n")

        # repositions SP of the caller
        return_command += ("//SP = ARG + 1\n"
                           "@ARG\n"
                           "D=M\n"
                           "@1\n"
                           "D=D+A\n"
                           "@SP\n"
                           "M=D\n\n")

        # restores THAT of the caller
        return_command += ("//THAT = *(endFrame - 1)\n"
                           "@FRAME\n"
                           "D=M\n"
                           "@1\n"
                           "A=D-A\n"
                           "D=M\n"
                           "@THAT\n"
                           "M=D\n\n")

        # restores THIS of the caller
        return_command += ("//THIS = *(endFrame - 2)\n"
                           "@FRAME\n"
                           "D=M\n"
                           "@2\n"
                           "A=D-A\n"
                           "D=M\n"
                           "@THIS\n"
                           "M=D\n\n")

        # restores ARG of the caller
        return_command += ("//ARG = *(endFrame - 3)\n"
                           "@FRAME\n"
                           "D=M\n"
                           "@3\n"
                           "A=D-A\n"
                           "D=M\n"
                           "@ARG\n"
                           "M=D\n\n")

        # restores LCL of the caller
        return_command += ("//LCL = (endFrame - 4)\n"
                           "@FRAME\n"
                           "D=M\n"
                           "@4\n"
                           "A=D-A\n"
                           "D=M\n"
                           "@LCL\n"
                           "M=D\n\n")

        # goes to return address in the caller's code
        return_command += ("//goto retAddr\n"
                           "@R14\n"
                           "A=M\n"
                           "0;JMP\n")

        return return_command

    @classmethod
    def __generate_stack_command(cls, command, segment, value):
        command_type = CommandType(command)

        if command_type is CommandType.C_PUSH:
            return cls.__push_command(segment, value)
        elif command_type is CommandType.C_POP:
            return cls.__pop_command(segment, value)

    @classmethod
    def __update_current_call_index(cls, function_name):
        if function_name in cls.call_command_number_map:
            cls.call_command_number_map[function_name] += 1
        else:
            cls.call_command_number_map[function_name] = 0

        cls.call_command_number = cls.call_command_number_map[function_name]

    @classmethod
    def __push_command(cls, segment, value):
        command = ''

        if segment == 'constant':
            command += ("@{}\n"
                        "D=A\n").format(str(value))
        elif segment == 'temp':
            command += ("@{}\n"
                        "D=M\n").format(str(5 + value))
        elif segment == 'static':
            command += ("@{}\n"
                        "D=M\n").format(cls.filename + '.' + str(value))
        elif segment == 'pointer':
            pointer = segment + ' ' + str(value)
            command += ("{}\n"
                        "D=M\n").format(cls.segment_mappings[pointer])
        else:
            command += ("{}\n"
                        "D=M\n"
                        "@{}\n"
                        "A=A+D\n"
                        "D=M\n").format(cls.segment_mappings[segment], str(value))

        # put value on a stack
        command += cls.__push_to_stack()
        return command

    @classmethod
    def __pop_command(cls, segment, index):
        pointer = segment + ' ' + str(index)
        command = ''

        if segment not in cls.segment_mappings or pointer in cls.segment_mappings:
            command += ("@SP\n"         # select stack (constant) base memory address
                        "AM=M-1\n"      # go one address back (pop)
                        "D=M\n")        # get value from that address

            if segment == 'static':
                command += ("@{}\n").format(cls.filename + '.' + str(index)) # select location where to pop value
            elif segment == 'temp':
                command += ("@{}\n").format(str(5 + index))  # select location where to pop value
            elif segment == 'pointer':
                command += ("{}\n").format(cls.segment_mappings[pointer]) # select location where to pop value

            command += ("M=D\n")                             # save value popped from the stack
        else:
            command += cls.__pop_from_stack(segment, index)

        return command

    @classmethod
    def __push_to_stack(cls):
        return ("@SP\n"
                "AM=M+1\n"
                "A=A-1\n"
                "M=D\n")

    @classmethod
    def __pop_from_stack(cls, segment, index):
        return ("{}\n"     # select segment's base memory address
                "D=M\n\n"  # get value of the segment's base address
                "@{}\n"    # select location where to pop value
                "D=D+A\n"  # get address where to pop value = RAM[*segmentPointer + i]
                "@R13\n"   # select general purpose register
                "M=D\n"    # put address where to pop value for temporary saving
                "@SP\n"    # select stack (constant) base memory address
                "AM=M-1\n" # go one address back (pop)
                "D=M\n"    # get value from that address
                "@R13\n"   # select general purpose register (where we temporarily stored address where to pop value)
                "A=M\n"    # get address where the poped value should be saved
                "M=D\n"    # save value popped from the stack
                ).format(cls.segment_mappings[segment], str(index))

    @classmethod
    def set_file_name(cls, filename):
        cls.filename = filename

    @classmethod
    def __is_logical(cls, command):
        return command in cls.logical_commands

    @classmethod
    def __is_arithmetic(cls, command):
        return command in cls.arithmetic_commands

    @classmethod
    def __is_stack_command(cls, command):
        return command in cls.stack_commands

    @classmethod
    def __is_negation(cls, command):
        return command in cls.negation_commands

    @classmethod
    def __is_branching(cls, command):
        return command in cls.branching_commands

    @classmethod
    def __is_function(cls, command):
        return command in cls.function_commands
