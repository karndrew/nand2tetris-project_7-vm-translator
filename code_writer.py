from instruction.command_type import CommandType
from instruction.command import Command

#
# Generates assomly code from the parsed VM command
#
class CodeWriter:

    #
    # Opens the output file / stream and gets ready to write it.
    #
    def __init__(self, filename):
        self.__open_program_file(filename)

    #
    # Informs the code writer that the translation of a new file has started
    # (called by the main program of the VM translator).
    #
    def set_file_name(self, filename):
        if filename.endswith('.vm'):
            filename = filename.replace(".vm", '')

        Command.set_file_name(filename[filename.rindex('/') + 1:len(filename)])

    #
    # Writes the assembly instructions that effect the bootstrap code that
    # initializes the VM. This code must be placed at the beginning
    # of the generated *.asm file.
    #
    def write_init(self):
        self.machine_program.write('//' + 'Bootstrap code\n')

        bootstrap_code = ("@256\n"
                          "D=A\n"
                          "@SP\n"
                          "M=D\n"
                          "@Sys.init\n"
                          "0;JMP\n")

        self.machine_program.write(bootstrap_code + '\n')

    #
    # Writes to the output file the assembly code that implements the given
    # arithmetic command.
    #
    def write_arithmetic(self, command):
        self.machine_program.write('//' + command + '\n')
        self.machine_program.write(Command.generate(command))
        self.machine_program.write('\n')

    #
    # Writes to the output file the assembly code that implements the given
    # command, where command is either C_PUSH or C_POP.
    #
    def write_push_pop(self, command, segment, index):
        if command is CommandType.C_PUSH:
            self.__write_push_command(segment, index)
        elif command is CommandType.C_POP:
            self.__write_pop_command(segment, index)

    #
    # Writes assembly code that effects the label command
    #
    def write_label(self, label):
        self.machine_program.write('//' + 'label ' + label + '\n')
        self.machine_program.write(Command.generate('label', label))
        self.machine_program.write('\n')

    #
    # Writes assembly code that effects the goto command
    #
    def write_goto(self, label):
        self.machine_program.write('//' + 'goto ' + label + '\n')
        self.machine_program.write(Command.generate('goto', label))
        self.machine_program.write('\n')

    #
    # Writes assembly code that effects the if-goto command
    #
    def write_if(self, label):
        self.machine_program.write('//' + 'if-goto ' + label + '\n')
        self.machine_program.write(Command.generate('if-goto', label))
        self.machine_program.write('\n')

    #
    # Writes assembly code that effects the function command
    #
    def write_function(self, function_name, num_vars):
        self.machine_program.write('//{} {} {}\n'.format('function', function_name, num_vars))
        self.machine_program.write(Command.generate('function', function_name, num_vars))
        self.machine_program.write('\n')

    #
    # Writes assembly code that effects the call command
    #
    def write_call(self, function_name, num_args):
        self.machine_program.write('//{} {} {}\n'.format('call', function_name, num_args))
        self.machine_program.write(Command.generate('call', function_name, num_args))
        self.machine_program.write('\n')

    #
    # Writes assembly code that effects the return command
    #
    def write_return(self):
        self.machine_program.write('//{}\n'.format('return'))
        self.machine_program.write(Command.generate('return'))
        self.machine_program.write('\n')

    #
    # Closes the output file.
    #
    def close(self):
        self.machine_program.close()

    def __write_push_command(self, segment, value):
        self.machine_program.write('//push ' + segment + ' ' + str(value) + '\n')
        self.machine_program.write(Command.generate('push', segment, value))
        self.machine_program.write('\n')

    def __write_pop_command(self, segment, index):
        self.machine_program.write('//pop ' + segment + ' ' + str(index) + '\n')
        self.machine_program.write(Command.generate('pop', segment, index))
        self.machine_program.write('\n')

    def __open_program_file(self, filename):
        self.machine_program = open(filename, 'a')
