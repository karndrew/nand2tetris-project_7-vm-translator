import sys
import os
from os import listdir

from parser import Parser
from code_writer import CodeWriter
from instruction.command_type import CommandType

class Translator:
    def __init__(self, filename):
        self.input_file = filename
        self.dir = ''
        self.output_file_name = self.__adjust_file_name(filename)

        self.__remove_file(self.output_file_name)
        self.code_writer = CodeWriter(self.output_file_name)

    def assemble(self):
        self.code_writer.write_init()
        self.code_writer.close()
        self.__handle_input(self.input_file)

    def __handle_input(self, file):
        if os.path.isfile(file) and file.endswith('.vm'):
            self.__handle_file(file)
        elif os.path.isdir(file):

            if self.dir:
                dir = self.dir + '/' + file
            else:
                dir = file

            file_list = os.listdir(file)

            for file_dir in file_list:
                self.__handle_input(dir + '/' + file_dir)

    def __handle_file(self, file):
        parser = Parser(file)
        self.code_writer = CodeWriter(self.output_file_name)
        self.code_writer.set_file_name(file)

        while parser.has_more_commands():
            parser.advance()

            command = parser.command_type()

            if command is CommandType.C_ARITHMETIC:
                self.code_writer.write_arithmetic(parser.arg1())
            elif command is CommandType.C_GOTO:
                self.code_writer.write_goto(parser.arg1())
            elif command is CommandType.C_IF:
                self.code_writer.write_if(parser.arg1())
            elif command is CommandType.C_LABEL:
                self.code_writer.write_label(parser.arg1())
            elif command is CommandType.C_CALL:
                self.code_writer.write_call(parser.arg1(), parser.arg2())
            elif command is CommandType.C_FUNCTION:
                self.code_writer.write_function(parser.arg1(), parser.arg2())
            elif command is CommandType.C_RETURN:
                self.code_writer.write_return()
            elif command is CommandType.C_PUSH or command is CommandType.C_POP:
                self.code_writer.write_push_pop(parser.command_type(), parser.arg1(), parser.arg2())

        self.code_writer.close()

    def __remove_file(cls, filename):
        if os.path.exists(filename):
            os.remove(filename)

    def __adjust_file_name(self, filename):
        if filename.endswith('.vm'):
            filename = filename.replace(".vm", ".asm")
        else:
            file = filename[filename.rindex('/') + 1:len(filename)] + '.asm'
            dir = filename + '/'
            filename = dir + file

        return filename

def main():
    assembler = Translator(sys.argv[1])
    assembler.assemble()



if __name__ == "__main__":
    main()
