from instruction.command_type import CommandType

#
# Parses each VM command into its lexical elements
#
# - Handles the parsing of a single .vm file
# - Reads a VM command, parses the command int its lexical components, and provides
# convinient access to these components.
# - Ignores all white space and comments
#
class Parser:

    def __init__(self, vm_filename):
        self.program_lines = self.__read_program_file(vm_filename)
        self.comm_type = None
        self.argument1 = None
        self.argument2 = None

    #
    # Are there more commands in the input?
    #
    def has_more_commands(self):
        return len(self.program_lines) > 0

    #
    # Reads the next command from the input and makes it the current command.
    # Should be called only if has_more_commands() is true. Initially there
    # is no current command.
    #
    def advance(self):
        if self.has_more_commands():
            line = self.program_lines.pop(0).strip()

            if not self.__is_white_space(line):
                line = self.__strip_inline_comment(line)
                self.__split_commands(line)
            else:
                self.comm_type = None
                self.argument1 = None
                self.argument2 = None

    #
    # Returns a constant representing the type of the current command.
    # C_ARITHMETIC is returned for all the arithmetic/logical commands.
    #
    def command_type(self):
        return self.comm_type

    #
    # Returns the first argument of the current command. In the case of
    # C_ARITHMETIC, the command itself (add, sub, etc.) is returned.
    # Should not be called if the current command is C_RETURN.
    #
    def arg1(self):
        if self.comm_type is not CommandType.C_RETURN:
            return self.argument1
        else:
            return None

    #
    # Returns the second argument of the current command. Should be called
    # only if the current command is C_PUSH, C_POP, C_FUNCTION, or C_CALL.
    #
    def arg2(self):
        if self.comm_type is CommandType.C_PUSH or \
        self.comm_type is CommandType.C_POP or \
        self.comm_type is CommandType.C_FUNCTION or \
        self.comm_type is CommandType.C_CALL:
            return self.argument2
        else:
            return None

    def __split_commands(self, line):
        command_args = line.split(' ')

        command = command_args[0].strip()
        self.__set_command_type(command)

        if len(command_args) == 1:
            self.argument1 = command

        elif len(command_args) == 2:
            self.argument1 = command_args[1]

        elif len(command_args) == 3:
            self.argument1 = command_args[1]
            self.argument2 = int(command_args[2].strip())

    def __set_command_type(self, command):
        if CommandType.is_arithmetic(command):
            self.comm_type = CommandType.C_ARITHMETIC
        else:
            self.comm_type = CommandType(command)

    def __is_blank(self, string):
        if string and string.strip() and not self.__is_line_break(string):
            # string is not None AND string is not empty or blank
            return False
        # string is None OR string is empty or blank
        return True

    def __is_line_break(self, string):
        if string == '\n' or string == '\r':
            return True
        return False

    def __is_comment(self, line):
        if line.strip().startswith("//"):
            return True
        return False

    def __is_white_space(self, line):
        return self.__is_blank(line) or self.__is_comment(line)

    def __strip_inline_comment(self, line):
        items = line.split("//")

        if len(items) > 1:
            return items[0].strip()

        return line

    def __read_program_file(self, filename):
        program = open(filename, 'r')
        file_lines = [line for line in program.readlines() if line.strip()]

        return list(file_lines)
